# Contracting

> On contracting as an independent software developer working in Czech Republic and invoicing within the European Union

This post doesn't cover starting and running a company.

- [ ] Translate this to English to serve as a guide for foreigners

- [Basic info](https://prace.rovnou.cz/zivnostnak.html)
- [Jak podnikat](http://www.jakpodnikat.cz/)

[OSVČ](#osvč) ziskava zivnostenske opravneni a ma [IČO](#ičo), pripadne i [DIČ](#dič),
pokud je platce [DPH](#dph) nebo [identifikovana osoba](#identifikovana-osoba).

V pripade fakturovani do zahranici ma [OSVC](#osvc) povinnost state se [identifikovanou osobou](#identifikovana-osoba).

V pripade zisku presahujicich hranici `1 000 000 CZK` v po sobe nasledujicich 12 mesicich ma OSVC povinnost stat se platcem [DPH](#dph).

## OSVC jako forma podnikani na hlavni cinnost pro programatora zijiciho v Ceske Republice a fakturujiciho do zahranici

Programovani je typ zivnosti volne. Nemusi se dokazovat odborna zpusobilost, vzdelani nebo praxe (…).

Registrace probiha vyplnenim jednotneho registracniho formulare (JRF) na centralnim registracnim miste (CRM).
Zivnostenske urady jsou vzdy CRM.

Dojde k:

- Registraci platce dane z prijmu
- Oznameni zdravotni pojistovne
- Oznamena sprave socialniho zabezpeceni (libovolna OSSZ, spada pod CSSZ)

## Povinnosti OSVC

- Mesicne platit zalohy na socialni (duchodove a nemocenske) pojisteni (k 8. dni nasledujiciho mesice)
- Mesicne platit zalohy na zdravotni pojisteni (k 8. dni nasledujiciho mesice)
- Uhradit zalohu na dan z prijmu k polovine brezna
- Podat danove priznani k 04/01
- Podat prehled prijmu na spravu socialniho zabezpeceni k 05/01
- Podat prehled prijmu na zdravotni pojistovnu k 05/01
- Uhradit zalohu na dan z prijmu k polovine cervna
- Uhradit zalohu na dan z prijmu k polovine zari
- Uhradit zalohu na dan z prijmu k polovine prosince

[Vice info o prehledech](http://www.jakpodnikat.cz/prehled-pojisteni.php)

[Vice info o zaloze na dan z prijmu](http://www.financnisprava.cz/cs/dane/dane/dan-z-prijmu/fyzicke-osoby-poplatnik/podnikatel-osvc#zalohy)

## Zalohy na zdravotni pojisteni

Pro jistotu poslat `3 000 CZK` kazdy mesic.

- [ ] Doplnit informace pro vypocet presne castky

## Zalohy na socialni pojisteni (duchodove a nemocenske)

Pro jistotu poslat `3 000 CZK` kazdy mesic.

- [ ] Doplnit informance pro vypocet presne castky

## Identifikovana osoba

http://www.jakpodnikat.cz/identifikovana-osoba-k-dph.php

## Souhrnne hlaseni

http://www.jakpodnikat.cz/souhrnne-hlaseni-dph.php

- Submit through [EPO](https://adisepo.mfcr.cz/adistc/adis/idpr_epo/epo2/uvod/vstup.faces)
- This is the form: [DPHSHV](https://adisepo.mfcr.cz/adistc/adis/idpr_epo/epo2/form/form_uvod.faces?pisemnost=DPHSHV)

- [x] 2017 1-January
- [x] 2017 2-February
- [x] 2017 3-March
- [x] 2017 4-April
- [x] 2017 5-May
- [x] 2017 6-June
- [x] 2017 7-July
- [x] 2017 8-August
- [x] 2017 9-September


## Kalendar OSVC

- 12 plateb zaloh za socialni pojisteni
- 12 plateb zaloh za zdravotni pojisteni

- `04/01` Podani danoveho priznani za predchozi kalendarni rok
    - Ziskat doklad o dni podani danoveho priznani potvrzeny financnim uradem
- `05/01` Podani prehledu za socialni pojisteni (do mesice od podani danoveho priznani)
    - Vyplnit spravne minimalni vymerovaci zaklad
    - Podat spolecne doklad o dni podani danoveho priznani potvrzeny financnim uradem
- `05/01` Podani prehledu za zdravotni pojisteni (do mesice od podani danoveho priznani)
    - Vyplnit spravne minimalni vymerovaci zaklad
    - Podat spolecne doklad o dni podani danoveho priznani potvrzeny financnim uradem
- `05` Zacit platit nove vyse zaloh podle noveho vymerovaciho zakladu podle prehledu od mesice podani prehledu

## Pokuty

- Nedolozeni prehledu za zdravotni pojisteni: 50 000 CZK

- [ ] Ozdrojovat

## Cisla uctu

- [ ] Doplnit

## Zálohy

Každý OSVČ musí platit měsíční zálohy (za co?).
Je možné na zálohách platit zákonné minimum.

V březnu se podává:

- Daňové přiznání
- Přehled OSVČ (z toho vychází minimální výše záloh)
- Přiznání a přehledy na sociální správu
- Přiznání a přehledy na zdravotní pojišťovnu

OSVČ musí platit ze zákona:

- Zdravotní pojištění
- Sociální pojištění

## Zalohy

Pocitaji se z [vyměřovacího základu](#vyměřovací-základ).

Upravuje odstavec 38a [zakona o danich z prijmu 568/1992](https://www.zakonyprolidi.cz/cs/1992-586#p38a).

[*Kolik si mam davat na zalohy (OSVC)*](https://www.bkpfinance.cz/kolik-si-mam-davat-na-zalohy-osvc)

### Zálohy na sociální pojištění

Pojistné na důchodové pojištění.

Mesicni vymerovaci zaklad pro socialni pojisteni je 1/4 [prumerne mzdy](#prumerna-mzda), tedy asi 7 500 CZK.
Zalohy na socialni pojisteni jsou zhruba 30 % mesicniho vyměřovacího základu, tedy asi 2 300 CZK.


- [ ] Ozdrojovat podle zakona

### Zalohy na nemocenske pojisteni

Mesicni platba 115 CZK.

### Zálohy na zdravotní pojištění

Mesicni vymerovaci zaklad pro zdravotni pojisteni je 1/2 [prumerne mzdy](#prumerna-mzda), tedy asi 15 000 CZK.
Zalohy na zdravotni pojisteni jsou zhruba 15 % mesicniho vyměřovacího základu, tedy asi 2 300 CZK.

- [ ] Ozdrojovat podle zakona

## Vyměřovací základ

Je odvozen z [neupraveného základu daně](#neupravený-základ-daně).
Aktuálně činí 50 % neupraveného základu daně (`prijmy-vydaje`?).

Existuje [minimalni vymerovaci zaklad](#minimalni-vyměřovací-základ) - z nej vychazeji minimalni zalohy.

- [ ] Ozdrojovat ze zakona

### Minimalni vyměřovací základ

Je odvozen od [prumerne mzdy](#prumerna-mzda).

## Prumerna mzda

Prumerna mzda je zhruba 30 000 CZK a je stanovena vyhlaskou Ministerstvem práce a sociálních věcí ČR.

Kdyz se zmeni, zmeni se i minimalni [vyměřovací základ](#vyměřovací-základ) a minimalni [zalohy](#zalohy).

## Neupravený základ daně

Zaklad dane pred upravou o nezdanitelne casti zakladu dane a o odecitatelne a pricitatelne polozky.
Dan se vypocitava z [upraveneho zakladu dane](#upraveny-zaklad-dane).

Tz. `prijmy - vydaje`?

- [ ] Ozdrojovat ze zakona

## Upravený základ daně

[Neupraveny zaklad dane](#neupraveny-zaklad-dane) po úpravě o nezdanitelné části základu daně, odčitatelné a přičitatelné položky.
Je to částka, ze které se vypočítává daň.

## CSSZ

## OSSZ

## Přehled o příjmech a výdajích OSVČ pro CSSZ

http://www.cssz.cz/cz/o-cssz/informace/nejcastejsi-dotazy/nejcastejsi-dotazy-osvc.htm#2

## e-Portal CSSZ

https://eportal.cssz.cz/


### Registrace

Bud na CRM (centralni registracni misto) pres JRF (jednotny registracni formular) nebo rucne:

- U spravce dane (na financnim urade)
- Na zdravotni pojistovne (nejcasteji VZP)
- Na okresni sprave socialniho zabezpeceni

Ziskam vypis ze zivnostenskeho listu (a ICO).

### Povinnosti

Vuci CSSZ:

- Platba 

## Sbirka Zakonu

[Sbirka zakonu na MVCR](http://aplikace.mvcr.cz/sbirka-zakonu/)

Absolutne nepouzitelne, nelze v tom poradne vyhledavat.

Nevladni alternativy:

### [ZakonyProLidi.cz](https://www.zakonyprolidi.cz/)

**Doporucuji**

- Ma HTTPS
- Lze odkazovat na zakon z jeho cisla: `/cs/{rok}-{cislo}`
- Lze odkazovat na paragrafy nebo casti z jejich cisla: `#p{cislo}`, `#cast{cislo}`
- V zakladni verzi zdarma, placena verze (zahrnujici nektere zakony) za 100 CZK/mesic nebo 1000 CZK/rok

### [Mesec.cz Zakony](https://www.mesec.cz/zakony/)

- Ma HTTPS
- Nelze odkazovat na zakon z jeho cisla
- Nelze odkazovat na paragrafy nebo casti z jejich cisla

### [Podnikatel.cz Zakony](https://www.podnikatel.cz/zakony)

- Ma HTTPS
- Nelze odkazovat na zakon z jeho cisla
- Nelze odkazovat na paragrafy nebo casti z jejich cisla

### [Finance.cz Zakony](https://www.finance.cz/dane-a-mzda/zakony/)

- Ma HTTPS
- Nelze odkazovat na zakon z jeho cisla
- Nelze odkazovat na paragrafy nebo casti z jejich cisla

### [Kurzy.cs Zakony](https://zakony.kurzy.cz/)

- Ma HTTPS
- Nelze odkazovat na zakon z jeho cisla
- Nelze odkazovat na paragrafy nebo casti z jejich cisla

### [Zakony.cz](http://www.zakony.cz/)

- Nema HTTPS
- Nelze odkazovat na zakon z jeho cisla
- Nelze odkazovat na paragrafy nebo casti z jejich cisla

### [Zakony-online.cz](http://zakony-online.cz/) (=[Ley](http://ley.cz/))

- Nema HTTPS
- Nelze odkazovat na zakon z jeho cisla
- Nelze odkazovat na paragrafy nebo casti z jejich cisla

### [ZakonyCR.cz](http://www.zakonycr.cz/)

- Nema HTTPS
- Nelze odkazovat na zakon z jeho cisla
- Nelze odkazovat na paragrafy nebo casti z jejich cisla

## Glossary

### OSVČ

> Osoba samostatně výdělečně činná

### IČO

> Identifikační číslo osoby

Disponuje jim podnikajici fyzicka osoba.

### DIČ

> Danove identifikacni cislo

Pro identifikovane osoby je to `CZ` + rodne cislo, napr. `CZ9212172981`.

### ARES

> Akumulovany (?) registr ekonomickych subjektu

[ARES](http://wwwinfo.mfcr.cz/ares/ares_es.html.cz)

Pouziva se k vyhledavani ekonomickych subjektu, v kontextu [OSVČ](#osvč) k:

- Vyhledani ICO podle jmena: `tomáš hübelbauer` -> `03003973`
- Vyhledani jmena podle ICO: `03003973` -> `tomáš hübelbauer`

Odkazuje na:

- Registr ekonomickych subjektu (RES)
- Registr zivnostenskeho podnikani (RZP)
- Registr platcu DPH (DPH)

### JRF

> Jednotny registracni formular

Umoznuje zaregistrovat se jako OSVC jednim formularem. Odpada potreba:

- Dodat vypis trestniho rejstriku (urad si jej ziska sam)
- Prihlasit se k placeni dane z prijmu k FU (urad vas prihlasi sam)
- Ohlasit zacatek podnikani na zdravotni pojistovne (urad to ohlasi za vas)
- Ohlasit zacatek podnikani na sprave socialniho zabezpeceni (urad to ohlasi za vas)

### CRM

> Centralni registracni misto

Kazdy zivnostensky urad je CRM.

### CSSZ

> Ceska sprava socialniho zabezpeceni

### OSSZ

> Okresni sprava socialniho zabezpeceni


### VZP

> Verejna zdravotni pojistovna

Ceska statni pojistovna, nejrozsirejnejsi pojistovna v Ceske Republice.

### DPH

> Dan z pridane hodnoty

### Identifikovana osoba

### Hlavni cinnost

Pracovat ciste jako OSVC, veskery prijem je z faktur vydanych.

### Vedlejsi cinnost

Pracovat castecne jako OSVC a castecne jako zamestnanec.
Tento post zohlednuje pouze situaci, kdy OSVC provozuje hlavni cinnost.

### Moje VZP

Online aplikace od VZP.

- Moznost sledovani stavu pojisteni (vcetne stavu pojistneho a penale)
- Moznost sledovani vykazane pece

### VZP Point

Online aplikace od VZP.

- Moznost podani formularu:
    - Platby pojistného a penále pojištěnce
    - Přehled o příjmech a výdajích OSVČ
    - Vyúčtování pojištěnce
    - Přehled vykázané péče na pojištěnce
    - Údaje o pojištěnci
    - Obecné podání
- Moznost prochazet podane formulare
- Moznost overeni platnosti prukazu pojistence

### OSSZ e-Portal

Online aplikace od OSSZ.

- Moznost podani formularu:
    - Přehled o příjmech a výdajích OSVČ
    - Přehled o výši pojistného
    - Žádost o zaslání informativního osobního listu důchodového pojištění
    - Náhled na inventuru pohledávek OSVČ za kalendářní rok
    - Potvrzení o bezdlužnosti fyzických osob – OSVČ a zaměstnavatelů

### EPO

Online aplikace pro podani souhrnneho hlaseni.

### DS

> Datova schranka

### ISDS

> Integrovany system datovych schranek

### DIS

> Danova informacni schranka
